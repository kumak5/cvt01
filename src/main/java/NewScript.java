import com.google.gson.Gson;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class NewScript {

    public static void main(String[] args) {
        String userId = args[0];

        var client = HttpClient.newHttpClient();

        var httpRequest = HttpRequest.newBuilder()
                .uri(URI.create(String.format("https://reqres.in/api/users/%s", userId)))
                .GET()
                .build();

        try{
            var response = client.send(httpRequest, HttpResponse.BodyHandlers.ofString());
            if (response.statusCode() == 404) throw new Exception("User not found!");
            //System.out.println(response.body());

            Data obj = new Gson().fromJson(response.body(), Data.class);

            System.out.println(String.format("%s %s", obj.data.first_name, obj.data.last_name));



        }catch(Exception e){
            e.printStackTrace();
        }


    }

}

class Data
{
    UserDTO data;
}

class UserDTO
{
    String id;
    String email;
    String first_name;
    String last_name;
    String avatar;
}

